![Screenshot preview of the about page "Vengeance" by glenthemes](./vengeance_pv.gif)

**Name:** About Page [01]: Vengeance  
**Author:** @&hairsp;glenthemes  
**Post:** [glenthemes.tumblr.com/post/153285961469](https://glenthemes.tumblr.com/post/153285961469)

##### Corvo Version: 
**Preview:** [glenthpvs.tumblr.com/vcorvo](https://glenthpvs.tumblr.com/vcorvo)  
**Download:** [pastebin.com/thG5PQxR](https://pastebin.com/thG5PQxR)

##### Emily Version: 
**Preview:** [glenthpvs.tumblr.com/vemily](https://glenthpvs.tumblr.com/vemily)  
**Download:** [pastebin.com/8zGjsuvt](https://pastebin.com/8zGjsuvt)
